﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace NETCoreWebsite
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                //// See documentation for Configuration support at https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-2.2
                //// The commented lines below are redundant due to DI being able to find that the service is needed. 
                //.ConfigureAppConfiguration((hostingContext, config) =>
                //{
                //    config.SetBasePath(Directory.GetCurrentDirectory());
                //})
                .UseStartup<Startup>()
                .Build();
    }
}
