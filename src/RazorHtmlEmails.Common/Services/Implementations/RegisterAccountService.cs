using Microsoft.AspNetCore.Hosting;
using MimeKit;
using MimeKit.Text;
using RazorHtmlEmails.RazorClassLib.Services;
using RazorHtmlEmails.RazorClassLib.Views.Emails.ConfirmAccount;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace RazorHtmlEmails.Common.Services.Implementation
{
    public class RegisterAccountService : IRegisterAccountService
    {
        private readonly IRazorViewToStringRenderer _razorViewToStringRenderer;
        private readonly IHostingEnvironment _env;

        public RegisterAccountService(IRazorViewToStringRenderer razorViewToStringRenderer,
                                        IHostingEnvironment env)
        {
            _razorViewToStringRenderer = razorViewToStringRenderer;
            _env = env;
        }

        public async Task Register(string email, string baseUrl)
        {
            // TODO: Validation + actually add the User to a DB + whatever else
            // TODO: Base URL off of ASP.NET Core Identity's logic or some other mechanism, rather than hardcoding to creating a random guid
            var confirmAccountModel = new ConfirmAccountViewModel($"{baseUrl}/{Guid.NewGuid()}");

            string body = await _razorViewToStringRenderer.RenderViewToStringAsync("/Views/Emails/ConfirmAccount/ConfirmAccount.cshtml", confirmAccountModel);

            var toAddresses = new List<string> { email };

            SendEmail(toAddresses, "donotreply@contoso.com", "Confirm your Account", body);
        }

        // TODO: In reality, you probably want to make an EmailService that houses this code, but #Demoware
        private async void SendEmail(List<string> toAddresses, string fromAddress, string subject, string body)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress(fromAddress));
            foreach (var to in toAddresses)
            {
                message.To.Add(new MailboxAddress(to));
            }
            message.Subject = subject;

            message.Body = new TextPart(TextFormat.Html)
            {
                Text = body
            };

            if (_env.IsDevelopment())
            {
                using (StreamWriter stream = new StreamWriter(@"D:\email.html", false))
                {
                    await stream.WriteAsync(message.Body.ToString());
                }
            }

            // using (var client = new SmtpClient())
            // {
            //     // For demo-purposes, accept all SSL certificates
            //     client.ServerCertificateValidationCallback = (s, c, h, e) => true;

            //     client.Connect("127.0.0.1", 25, false);

            //     client.Send(message);
            //     client.Disconnect(true);
            // }
        }
    }
}